

#include <stdio.h>
#include <termios.h>
#include <unistd.h>

//id-20190802-102254 termbin y79a 

int main()
{
        printf( "lfkey:\n" );

        struct termios ot;
        if(tcgetattr(STDIN_FILENO, &ot) == -1) perror(")-");

        struct termios t = ot;
        t.c_lflag &= ~(ECHO | ICANON);
        t.c_cc[VMIN] = 1;
        t.c_cc[VTIME] = 0;
        if(tcsetattr(STDIN_FILENO, TCSANOW, &t) == -1) perror(")-");

        int a;
        while(a = getchar(), a != 'q') {
                fprintf(stderr, "you pressed '%c' with keycode %d.\n", a, a );
        }

        if(tcsetattr(STDIN_FILENO, TCSANOW, &ot) == -1) perror(")-");

        return 0;
}


